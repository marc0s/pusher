use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :push_registry_web, PushRegistryWeb.Endpoint,
  http: [port: 4002],
  server: false

config :push_registry_web,
  port: 4002,
  push_registry: PushRegistry.Registration.Mock


config :push_registry, PushRegistry.Repo,
  database: "push_registry_test",
  username: "pusher",
  password: "secret",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# disable logger outputs while running tests
config :logger, backends: []

# we want to print the test cases' names and exclude those marked as :skip
config :ex_unit,
  trace: true,
  exclude: [:skip]
