# Compatibility notes for legacy HTTP endpoint

## Endpoints

* POST /push_appserver/v1/register: SUPPORTED
* POST /push_appserver/v1/unregister: SUPPORTED
* POST /push_appserver/v1/push: NOT IMPLEMENTED
* GET /push_appserver/v1/settings: NOT SUPPORTED
* GET /push_appserver/v1/settings/:device_uuid: NOT SUPPORTED
* GET /push_appserver/v1/health: NOT SUPPORTED

### Legend

SUPPORTED: implemented and working
NOT IMPLEMENTED: not implemented but planned
NOT SUPPORTED: not expected to be supported

## Alternatives

NOT SUPPORTED endpoints may have alternatives in the new API.
