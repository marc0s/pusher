# Pusher

A (mobile) push notifications' hub.

With `pusher` you can centralize all your push notifications needs for your
backend services.  All of them can tell `pusher` to send push notifications to
your registered clients.

# Architecture

`pusher` is composed of different elements that work together.  These elements
are:

* `push_registry`: where clients register themselves as push notifications
  targets.
* `push_sender`: the application that backends will use to actually send
  notifications to clients.  This is the piece that will interact with the
  different push notifications providers (namely APNS and FCM).
* `backends`: the "users" of `push_sender`, the services that need to send push
  notifications to registered clients.

The different backends will communicate with `push_sender` via a known interface
`push_client`.

## push_registry

The registry has the responsibility of keeping a list of registered clients.
That is, it will expose the interface to mobile clients to sign up for push
notifications and another interface for `push_sender` to check whether a push
notification sending request is aimed for a registered push client or not.

## push_sender

The `push_sender` will receive requests from the backends to send push
notifications to clients.  It will expose a common interface for all the
backends and will have, based on configuration, the different push notification
providers' clients to use for sending the actual push notifications.  That is,
this is the code responsible of calling an APNS or FCM client that pushes the
notification to the network.

## backends

Here's where most diversity will be found.  These are the different applications
that want to send notifications to clients.  Each of them has to be aware of the
interface exposed by `push_sender` and the specifics of the sending application.

For instance, in the case of XMPP, the backend might be an [XMPP
component](https://xmpp.org/extensions/xep-0114.html) implementing the
[XEP-0357](https://xmpp.org/extensions/xep-0357.html) part of the App Server and
the code that knows how to use `push_sender`'s interface.


# Implementation

As both a learning and exploring exercise `pusher` is implemented in
[Elixir](https://elixir-lang.org/).

## API

HIGHLY UNSTABLE! This is a WORK IN PROGRESS

### push_registry

* `create_registry/1`
* `get_registry!/1`
* `delete_registry/2`

# Contact

Ping [marcos@tenak.net](mailto:marcos@tenak.net)

