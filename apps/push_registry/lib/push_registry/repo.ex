defmodule PushRegistry.Repo do
  use Ecto.Repo,
    otp_app: :push_registry,
    adapter: Ecto.Adapters.Postgres
end
