defmodule PushRegistry.Registration.PushRegistry do
  use Ecto.Schema

  import Ecto.Changeset

  schema "push_registries" do
    field :node, :string
    field :token, :string
    field :provider, :string
    field :secret, :string
  end

  def changeset(push_registry, params \\ %{}) do
    push_registry
    |> cast(params, [:node, :token, :provider, :secret])
    |> validate_required([:node, :token, :provider])
    |> unique_constraint(:node)
    |> put_secret()
  end

  defp put_secret(%Ecto.Changeset{valid?: true} = changeset) do
    change(changeset, %{secret: gen_secret()})
  end
  
  defp put_secret(changeset) do
    changeset
  end

  defp gen_secret() do
    SecureRandom.hex(32)
  end
end
