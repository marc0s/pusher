defmodule PushRegistry.Repo.Migrations.RenameTypeToProvider do
  use Ecto.Migration

  def change do
    rename(table(:push_registries), :type, to: :provider)
  end
end
