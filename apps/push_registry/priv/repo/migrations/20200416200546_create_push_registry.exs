defmodule PushRegistry.Repo.Migrations.CreatePushRegistry do
  use Ecto.Migration

  def change do
    create table(:push_registries) do
      add :node, :string, null: false, primary_key: true
      add :token, :string, null: false
      add :type, :string, null: false
      add :secret, :string, null: false
    end
  end
end
