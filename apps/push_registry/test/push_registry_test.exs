defmodule PushRegistryTest do
  # This RepoCase is defined in support/ for having a ready DB connection
  use PushRegistry.RepoCase

  alias PushRegistry.Registration
  alias PushRegistry.Registration.PushRegistry

  @valid_attrs %{node: "some node", token: "some token", provider: "some provider"}
  @invalid_attrs %{node: "some node"}

  def preg_fixture(_attrs) do
    {:ok, push_registry} = Registration.create_registry(
      @valid_attrs.node,
      @valid_attrs.provider,
      @valid_attrs.token
    )

    push_registry
  end
  
  describe "create_registry/3" do

    test "with valid data succeeds" do
      assert {:ok, %PushRegistry{} = push_registry} = Registration.create_registry(
        @valid_attrs.node, @valid_attrs.provider, @valid_attrs.token
      )
      assert push_registry.node == "some node"
      assert push_registry.token == "some token"
      assert push_registry.provider == "some provider"
      assert byte_size(push_registry.secret) == 64
    end

    test "with invalid data throws" do
      assert {:error, _} = Registration.create_registry(
        @invalid_attrs.node, nil, nil
      )
    end
  end

  describe "get_registry!/1" do
    test "returns the registry with given node" do
      push_registry = preg_fixture(@valid_attrs)
      assert Registration.get_registry!(push_registry.node) == push_registry
    end

    test "fails if given node does not exist" do
      assert_raise Ecto.NoResultsError, fn ->
        Registration.get_registry!("invalid node")
      end
    end
  end

  describe "delete_registry/2" do
    test "deletes the registry with given node and secret" do
      push_registry = preg_fixture(@valid_attrs)
      assert {:ok, %PushRegistry{}} = Registration.delete_registry(push_registry.node, push_registry.secret)
    end
  end
end
