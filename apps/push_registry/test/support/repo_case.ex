defmodule PushRegistry.RepoCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias PushRegistry.Repo

      import Ecto
      import Ecto.Query
      import PushRegistry.RepoCase
    end
  end

  setup tags do
	  :ok = Ecto.Adapters.SQL.Sandbox.checkout(PushRegistry.Repo)

    unless tags[:async] do
	    Ecto.Adapters.SQL.Sandbox.mode(PushRegistry.Repo, {:shared, self()})
    end

    :ok
  end
end
