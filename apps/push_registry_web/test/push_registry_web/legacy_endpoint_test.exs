defmodule PushRegistryWeb.LegacyEndpointTest do
  use ExUnit.Case, async: true
  use Plug.Test
  import Mox

  setup :verify_on_exit!

  @opts PushRegistryWeb.LegacyEndpoint.init([])

  @valid_input %{
    type: "some type",
    node: "some node",
    token: "some token"
  }
  @valid_unregister %{
    node: "some node",
    secret: "some secret"
  }
  @invalid_input %{
    type: "foo",
    node: "some node",
    token: "some token"
  }
  @expected_output %{
    node: "some node",
    provider: "some type",
    secret: "some secret",
    token: "some token",
  }

  test "it returns settings" do
    conn = conn :get, "/v1/settings"
    conn = PushRegistryWeb.LegacyEndpoint.call(conn, @opts)
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "the settings list"
  end

  test "registers a user" do
    PushRegistry.Registration.Mock
    |> expect(:create_registry, fn (_node, _provider, _token) -> {:ok, @expected_output} end)
    conn =
      :post
      |> conn("/v1/register", @valid_input)
      |> PushRegistryWeb.LegacyEndpoint.call(@opts)
    assert conn.state == :sent
    assert conn.status == 200
    assert valid?(conn.resp_body)
  end

  test "refuses to register a user with invalid push type" do
    PushRegistry.Registration.Mock
    |> expect(:create_registry, fn (_node, _provider = "foo", _token) -> {:err, "Invalid push type"} end)
    conn =
      :post
      |> conn("/v1/register", @invalid_input)
      |> PushRegistryWeb.LegacyEndpoint.call(@opts)
    assert conn.state == :sent
    assert conn.status == 500
    assert error?(conn.resp_body, "Failed to register")
  end

  test "unregisters a user" do
    PushRegistry.Registration.Mock
    |> expect(:delete_registry, fn (node, secret) ->
      {:ok, %PushRegistry.Registration.PushRegistry{node: node, secret: secret}}
    end)
    conn =
      :post
      |> conn("/v1/unregister", @valid_unregister)
      |> PushRegistryWeb.LegacyEndpoint.call(@opts)
    assert conn.state == :sent
    assert conn.status == 200
    assert valid?(conn.resp_body)
  end

  defp valid?(body) do
    assert body == """
    OK
    #{@expected_output.node}
    #{@expected_output.secret}
    """
  end

  defp error?(body, message) do
    assert body == """
    ERROR
    #{message}
    """
  end
end
