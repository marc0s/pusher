defmodule PushRegistryWeb.EndpointTest do
  use ExUnit.Case, async: true
  use Plug.Test

  #alias PushRegistry.Registration.PushRegistry
  
  @opts PushRegistryWeb.Endpoint.init([])

  test "/version returns 1" do
    conn =
      :get
      |> conn("/version")
      |> PushRegistryWeb.Endpoint.call(@opts)
    assert conn.state == :sent
    assert conn.status == 200
    assert conn.resp_body == "1"
  end

  test "an invalid path gives a 404" do
    conn =
      :get
      |> conn("/notfound")
      |> PushRegistryWeb.Endpoint.call(@opts)
    assert conn.state == :sent
    assert conn.status == 404
    body = Jason.decode!(conn.resp_body)
    assert body == %{
      "error" => 404,
      "message" => "Not Found"
    }
  end

  test "can register a push client" do
    registry = %{"node" => "some node", "token" => "some token"}
    conn =
      :post
      |> conn("/registry", registry)
      |> PushRegistryWeb.Endpoint.call(@opts)
    assert conn.state == :sent
    assert conn.status == 201
    body = Jason.decode!(conn.resp_body)
    assert body["node"] == registry["node"]
    assert body["token"] == registry["token"]
    assert String.length(body["secret"]) > 0
  end
end
