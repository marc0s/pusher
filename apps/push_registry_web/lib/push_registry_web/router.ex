defmodule PushRegistryWeb.Router do
  @moduledoc false
  use Plug.Router

  plug Plug.Logger
  plug Plug.RequestId, http_header: "Request-Id"
  plug :match
  plug :dispatch

  forward("/api", to: PushRegistryWeb.Endpoint)
  forward("/push_appserver", to: PushRegistryWeb.LegacyEndpoint)
end
