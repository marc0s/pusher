defmodule PushRegistryWeb.LegacyEndpoint do
  @moduledoc """
  A Plug responsible for receiving legacy requests.

  Basically a Plug.Router.
  """

  use Plug.Router
  alias PushRegistry.Registration.PushRegistry

  @registration Application.get_env(:push_registry_web, :push_registry)

  plug :match
  plug Plug.Parsers, parsers: [:urlencoded]
  plug :dispatch

  get "/v1/settings" do
    send_resp(conn, 200, "the settings list")
  end

  post "/v1/register" do
    conn.body_params
    |> valid_for_register?()
    |> map_values_from_legacy()
    |> create_registry()
    |> case do
      {:ok, registry} ->
        send_resp(conn, 200, as_body(registry))
      {:err, _} ->
        send_resp(conn, 500, "ERROR\nFailed to register\n")
      end
  end

  post "/v1/unregister" do
    conn.body_params
    |> valid_for_unregister?()
    |> delete_registry()
    |> case do
        {:ok, %PushRegistry{}=registry} ->
          send_resp(conn, 200, as_body(registry))
        {:err, _} ->
          send_resp(conn, 500, "ERROR\nFailed to unregister")
      end
  end

  defp as_body(registry) do
    """
    OK
    #{registry.node}
    #{registry.secret}
    """
  end

  defp valid_for_register?(fields) do
    fields
  end
  defp valid_for_unregister?(fields) do
    fields
  end
  defp map_values_from_legacy(%{provider: _provider} = current), do: current
  defp map_values_from_legacy(%{} = legacy) do
    legacy
    |> Map.put_new("provider", Map.get(legacy, "type"))
    |> Map.drop(["type"])
  end

  defp create_registry(fields) do
    @registration.create_registry(fields["node"], fields["provider"], fields["token"])
  end

  defp delete_registry(fields) do
    @registration.delete_registry(fields["node"], fields["secret"])
  end
end
