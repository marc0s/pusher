defmodule PushRegistryWeb.Endpoint do
  @moduledoc """
  A Plug responsible for receiving /api requests.

  Basically a Plug.Router.
  """

  use Plug.Router

  plug Plug.Parsers, parsers: [:json], json_decoder: Jason
  plug :match
  plug :dispatch

  get "/version" do
    send_resp(conn, 200, "1")
  end

  post "/registry" do
    %{
      "node" => node,
      "token" => token
    } = conn.body_params
    body = %{
      "node" => node,
      "token" => token,
      "secret" => "some secret"
    }
    render_json(%{conn | status: 201}, body)
  end

  match _ do
    render_json(%{conn | status: 404}, %{"error" => 404, "message" => "Not Found"})
  end

  defp render_json(%{status: status} = conn, data) do
    body = Jason.encode!(data)
    conn
    |> put_resp_content_type("application/json")
    |> send_resp((status || 200),  body)
  end
end
