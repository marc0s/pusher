defmodule PushSender.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Starts a worker by calling: PushSender.Worker.start_link(arg)
      # {PushSender.Worker, arg}
      {PushRegistry.Repo, []},
      {Plug.Cowboy, scheme: :http, plug: PushRegistryWeb.Router, options: [
          port: Application.get_env(:push_registry_web, :port, 4000)
        ]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: PushSender.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
