defmodule PushSender do
  @moduledoc """
  Documentation for `PushSender`.
  """

  # TODO use real one
  defmodule PushSender.Notifier do
    def notify(_registry, _notification) do
      :ok
    end
  end

  @doc """
  Sends the given notification and acknowledges, or not, the sender.
  """
  def notify(sender_pid, %{node: node}=notification) do
    registry = PushRegistry.Registration.get_registry!(node)
    case PushSender.Notifier.notify(registry, notification) do
      :ok ->
        send sender_pid, {:ok}
      _ ->
        send sender_pid, {:error}
    end
  end
end
