# development notes

* apps/push_registry
```
Don't forget to add your new repo to your supervision tree
(typically in lib/push_registry/application.ex):

    {PushRegistry.Repo, []}

And to add it to the list of ecto repositories in your
configuration files (so Ecto tasks work as expected):

    config :push_registry,
      ecto_repos: [PushRegistry.Repo]
```
